import java.util.Scanner;

public class Utilities {
	private Scanner sc;
	
	public double[] getValues() {
		sc = new Scanner(System.in);
		double numOne = 0, numTwo = 0; 
		
		System.out.println("please enter first number: ");
		
		boolean gotIntOne = false;
		while (!gotIntOne) {
			if (sc.hasNextInt()) {
				numOne = Integer.parseInt(sc.next());
				gotIntOne = true;
			}
			else {
				System.out.println("Please enter a valid integer");
				sc.next();
			}
		}
		
		System.out.println("please enter Second number: ");
		boolean gotIntTwo = false;
		while (!gotIntTwo) {
			if (sc.hasNextInt()) {
				numTwo = Integer.parseInt(sc.next());
				gotIntTwo = true;
			}
			else {
				System.out.println("Please enter a valid integer");
				sc.next();
			}
		}
		double[] values = {numOne, numTwo};
		return values;
	}
	
	
	public int getOption() {
		
		System.out.println("please enter calculation: \n"
				+ "1 - add \n"
				+ "2 - subtract \n"
				+ "3 - multiply \n"
				+ "4 - division \n");
		int selection = 0;
		boolean hasAction = false;
		while (!hasAction) {
			if (sc.hasNextInt()) {
				selection = Integer.parseInt(sc.next());
				if (selection >= 1 && selection <= 4) {
					hasAction = true;
				}
				else {
					System.out.println("please enter a valid option");
				}
			}
		}
		sc.close();
		return selection;
	}
	public double add(double[] values) {
		double result = values[0] + values[1];
		return result;
	}
	public double subtract(double[] values) {
		double result = values[0] - values[1];
		return result;
	}
	public double multiply(double[] values) {
		double result = values[0] * values[1];
		return result;
	}
	public double division(double[] values) {
		double result = values[0] / values[1];
		return result;
	}
}
