import java.util.Scanner;

public class JavaCalcMain {
	public static void main(String[] args) {
		Utilities utils = new Utilities();
		double[] values = utils.getValues();
		int option  = utils.getOption();
		
		double result = 0;
		switch (option) {
		case 1:
			result = utils.add(values);
			break;
		case 2:
			result = utils.subtract(values);
			break;
		case 3:
			result = utils.multiply(values);
			break;
		case 4: 
			result = utils.division(values);
			break;
		}
		System.out.println("The result of this calculation is: " + result);
	}
	
}
